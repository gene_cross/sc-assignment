﻿using System.ServiceModel;

namespace RecoveryManagerEndpoints {

	[ServiceContract]
	public interface IRecoveryService {

		[OperationContract]
		void Decrypt(string hashedPwd, uint pwdLength, long start, long iterations, string charset);
	}

	[ServiceContract(CallbackContract = typeof(IRecoveryService))]
	public interface IRecoveryServiceCallback {

		[OperationContract]
		void AddService();

		[OperationContract]
		void RemoveService();

		[OperationContract]
		void ServicePutResult(string hashedPwd, string result, long block);
	}
}
