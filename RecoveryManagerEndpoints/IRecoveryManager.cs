﻿using System.ServiceModel;

namespace RecoveryManagerEndpoints {

	[ServiceContract]
	public interface IRecoveryManager {

		[OperationContract]
		void Decrypt(string hashedPwd, uint length);

		[OperationContract]
		void CancelDecryption(string hashedPwd);

		[OperationContract]
		string GetDecryptedPwd(string hashedPwd);

		[OperationContract]
		double GetProgress(string hashedPwd);

		[OperationContract]
		string GetNextEncryptedPwdToRecover(uint pwdLength);

		[OperationContract]
		string EncryptString(string pwdString);
	}
}
