﻿using System;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using RecoveryManagerEndpoints;

namespace WPFClient {

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	[CallbackBehavior(
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	public partial class MainWindow : Window {

		private delegate void voidDelegate();
		private delegate void DecryptDelegate(string hashedPwd, uint pwdLength);

		private NetTcpBinding tcpBinding;
		private IRecoveryManager recoveryManager;
		private string hashedPwd;
		private uint pwdLength;
		private bool isDecrypting;
		voidDelegate progDlg;


		public MainWindow() {
			InitializeComponent();

			// init
			hashedPwd = null;
			isDecrypting = false;
			progDlg = PollForProgress;
			progDlg.BeginInvoke(null, null);
		}

		private void wnd_main_Loaded(object sender, RoutedEventArgs args) {
			tcpBinding = new NetTcpBinding();

			tcpBinding.Security.Mode = SecurityMode.None;
			tcpBinding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
			tcpBinding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.None;
			tcpBinding.Security.Message.ClientCredentialType = MessageCredentialType.None;

			tcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
			tcpBinding.MaxReceivedMessageSize = Int32.MaxValue;
			tcpBinding.ReaderQuotas.MaxArrayLength = Int32.MaxValue;

			// connect to recovery manager
			try {
				var addr = Microsoft.VisualBasic.Interaction.InputBox("Enter the Recovery Manager Address", "Address Input", "net.tcp://localhost:8080/RecoveryManager", 0, 0);

				var recoveryManagerFactory = new ChannelFactory<IRecoveryManager>(tcpBinding, addr);
				recoveryManager = recoveryManagerFactory.CreateChannel();
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				MessageBox.Show(e.Message);
				Close();
			}
		}

		private void btn_getHashedPwd_Click(object sender, RoutedEventArgs args) {
			try {
				if (isDecrypting) {
					// clear current request
					btn_cancel_Click(sender, args);
				}
				pwdLength = Convert.ToUInt32(txtBox_pwdLength.Text);
				hashedPwd = recoveryManager.GetNextEncryptedPwdToRecover(pwdLength);

				lbl_hashedPwd.Content = "Hashed Password: " + hashedPwd;
				progressbar.Value = 0.0;
			}
			catch (FormatException formatExcept) {
				MessageBox.Show("Please enter a positive integer for the Password Length\n" + formatExcept.Message);
			}
			catch (OverflowException overflowException) {
				MessageBox.Show("The password length needs to be positive.\n" + overflowException.Message);
			}
			catch (Exception e) {
				MessageBox.Show("Connection error to server, Closing.\n" + e.Message);
				Console.WriteLine(e.Message);
				Close();
			}
		}

		private void btn_decrypt_Click(object sender, RoutedEventArgs args) {
			if (!isDecrypting) {
				voidDelegate dlg = StartDecryptionWrapper;
				dlg.BeginInvoke(null, null);

				isDecrypting = true;
				lbl_decryptedPwd.Content = "Decrypted Password: ";
			}
		}

		private void StartDecryptionWrapper() {
			try {
				if (hashedPwd != null) {
					recoveryManager.Decrypt(hashedPwd, Convert.ToUInt32(pwdLength));
				}
			}
			catch (FaultException faultExcept) {
				Console.WriteLine("FaultException: StartDecryptionWrapper\n" + faultExcept.Message);
			}
		}

		private void PollForProgress() {
			double progress = 0.0;

			try {
				while (true) {

					while (progress < 1.0) {
						if (isDecrypting) {
							Dispatcher.Invoke(() => {
								progressbar.Value = recoveryManager.GetProgress(hashedPwd);
								progress = progressbar.Value;
							});
							Console.WriteLine("progress: " + progress);
							Thread.Sleep(1000);
						}
					}

					if (isDecrypting) {
						Dispatcher.Invoke(() => {
							isDecrypting = false;
							progress = 0.0;
							lbl_decryptedPwd.Content = "Decrypted Password: " + recoveryManager.GetDecryptedPwd(hashedPwd);
						});
					}
				}
			}
			catch (FaultException faultExcept) {
				Console.WriteLine("FaultException: PollForProgess\n" + faultExcept.Message);
				MessageBox.Show("Connection to the server was lost.\n" + faultExcept.Message);
				Close();
			}
		}

		private void txtBox_pwdLength_GotFocus(object sender, RoutedEventArgs e) {
			txtBox_pwdLength.Text = "";
		}

		private void btn_cancel_Click(object sender, RoutedEventArgs e) {
			if (hashedPwd != null && isDecrypting) {
				voidDelegate cancelDlg = CancelWrapper;
				cancelDlg.BeginInvoke(null, null);
				isDecrypting = false;
				progressbar.Value = 0.0;
			}
		}

		private void CancelWrapper() {
			try {
				recoveryManager.CancelDecryption(hashedPwd);
			}
			catch (FaultException faultExcept) {
				Console.WriteLine("FaultException: CancelWrapper\n" + faultExcept.Message);
			}
		}
	}
}
