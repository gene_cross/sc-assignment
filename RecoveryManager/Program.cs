﻿using System;
using System.Net;
using System.ServiceModel;
using RecoveryManagerPasswordService;
using RecoveryManagerEndpoints;

namespace RecoveryManager {

	class Program {

		static void Main(string[] args) {
			var tcpBinding = new NetTcpBinding();

			tcpBinding.Security.Mode = SecurityMode.None;
			tcpBinding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
			tcpBinding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.None;
			tcpBinding.Security.Message.ClientCredentialType = MessageCredentialType.None;

			tcpBinding.MaxReceivedMessageSize = Int32.MaxValue;
			tcpBinding.ReaderQuotas.MaxArrayLength = Int32.MaxValue;

			try {
				// get host local ip
				string addr = "net.tcp://";
				var hostEntries = Dns.GetHostEntry(Dns.GetHostName());
				foreach (var ip in hostEntries.AddressList) {
					if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
						addr += ip.ToString();
						addr += ":8080/RecoveryManager";
						break;
					}
				}

				var recoveryManager = new RecoveryManager(new PwdRecoverer(), new ServiceHandler());

				var host = new ServiceHost(recoveryManager);
				host.AddServiceEndpoint(typeof(IRecoveryManager), tcpBinding, addr);
				host.AddServiceEndpoint(typeof(IRecoveryServiceCallback), tcpBinding, addr);

				host.Open();

				Console.WriteLine(" << Recovery Manager >>\nAddress: " + addr + "\nPress ENTER to exit:");
				Console.ReadLine();

				host.Close();
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
			}
		}
	}
}
