﻿namespace RecoveryManager {

	public interface IServiceHandler {

		void AddRecoveryService();
		void RemoveRecoveryService();
		void ServicePutResult(string hashedPwd, string result, long block);

		void Decrypt(string hashedPwd, uint pwdLength);
		string GetDecryptedPwd(string hashedPwd);

		double GetProgess(string hashedPwd);
		void Cancel(string hashedPwd);
	}
}
