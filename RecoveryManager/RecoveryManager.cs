﻿using System;
using System.ServiceModel;
using RecoveryManagerPasswordService;
using RecoveryManagerEndpoints;

namespace RecoveryManager {

	/// <summary>
	/// Server controller.
	/// </summary>
	[ServiceBehavior(
		InstanceContextMode = InstanceContextMode.Single,
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	[CallbackBehavior(
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	internal class RecoveryManager : IRecoveryManager, IRecoveryServiceCallback {

		private IPwdRecoverer pwdService;
		private IServiceHandler serviceHandler;

		private object lockObject;


		public RecoveryManager(IPwdRecoverer pwdService, IServiceHandler serviceHandler) {
			this.pwdService = pwdService;
			this.serviceHandler = serviceHandler;

			lockObject = new object();
		}

		public void Decrypt(string hashedPwd, uint pwdLength) {
			serviceHandler.Decrypt(hashedPwd, pwdLength);
		}
		
		public void CancelDecryption(string hashedPwd) {
			serviceHandler.Cancel(hashedPwd);
		}

		public string GetDecryptedPwd(string hashedPwd) {
			return serviceHandler.GetDecryptedPwd(hashedPwd);
		}

		public double GetProgress(string hashedPwd) {
			return serviceHandler.GetProgess(hashedPwd);
		}

		public string GetNextEncryptedPwdToRecover(uint pwdLength) {
			lock (lockObject) {
				return pwdService.GetNextEncryptedPwdToRecover(Convert.ToInt32(pwdLength));
			}
		}

		public string EncryptString(string pwdString) {
			lock (lockObject) {
				return pwdService.EncryptString(pwdString);
			}
		}

		public void AddService() {
			serviceHandler.AddRecoveryService();
		}

		public void RemoveService() {
			serviceHandler.RemoveRecoveryService();
		}

		public void ServicePutResult(string hashedPwd, string result, long block) {
			serviceHandler.ServicePutResult(hashedPwd, result, block);
		}
	}
}
