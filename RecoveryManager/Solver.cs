﻿using System;
using System.Collections.Generic;
using RecoveryManagerEndpoints;

namespace RecoveryManager {

	class Solver {

		private delegate void DecryptWrapperDelegate(IRecoveryService rs, long startBlock, long iterations);
		private delegate void CancelWrapperDelegate();

		private string decryptedPwd;
		private string hashedPwd;
		private string charset;
		private uint pwdLength;
		private bool finished = false;

		private const long BLOCKSIZE = 100000L;

		// nasty last block handling
		// last block of iterations will be smaller than BLOCKSIZE, need to store it's size
		private long lastIterationStart = -1;
		private long lastNumIterations;

		// used like a queue but easier to remove from
		private LinkedList<long> blockQueue = new LinkedList<long>();

		private long currentIteration;
		private long completedIterations;
		private long totalIterations;
		private long numBlocksRemaining;

		private object lockObject = new object();


		public Solver(string hashedPwd, string charset, uint pwdLength) {
			this.hashedPwd = hashedPwd;
			this.charset = charset;
			this.pwdLength = pwdLength;

			totalIterations = Convert.ToInt64(Math.Pow(charset.Length, pwdLength));

			// set the total number of blocks of iterations
			numBlocksRemaining = totalIterations / BLOCKSIZE;
			if ((totalIterations % BLOCKSIZE) != 0) {
				numBlocksRemaining += 1;
			}
		}

		// give an IRecoveryService to this solver to use
		public bool AddRecoveryService(IRecoveryService rs) {
			lock (lockObject) {
				if (finished) {
					return true;
				}
				DecryptWrapperDelegate decryptDlg = DecryptWrapper;

				// adding blocks to queue
				if (numBlocksRemaining > 0) {

					// if the next block is beyond the search space, init the last block
					if ((currentIteration + BLOCKSIZE) > totalIterations) {
						lastNumIterations = totalIterations - currentIteration;
						lastIterationStart = currentIteration;

						blockQueue.AddLast(lastIterationStart);
						decryptDlg.BeginInvoke(rs, lastIterationStart, lastNumIterations, null, null);
					}
					else {
						blockQueue.AddLast(currentIteration);
						decryptDlg.BeginInvoke(rs, currentIteration, BLOCKSIZE, null, null);
						currentIteration += BLOCKSIZE;
					}

					numBlocksRemaining -= 1;
				}
				// all blocks added to queue, iterate over remaining
				else {
					// if last block
					if (blockQueue.First.Value == lastIterationStart) {
						decryptDlg.BeginInvoke(rs, blockQueue.First.Value, lastNumIterations, null, null);
					}
					else {
						decryptDlg.BeginInvoke(rs, blockQueue.First.Value, BLOCKSIZE, null, null);
					}
					// move block to the back of the queue
					blockQueue.AddLast(blockQueue.First);
					blockQueue.RemoveFirst();
				}
				// return false
				return finished;
			}
		}

		private void DecryptWrapper(IRecoveryService rs, long startIteration, long numIterations) {
			try {
				rs.Decrypt(hashedPwd, pwdLength, startIteration, numIterations, charset);
			}
			catch (Exception e) {
				// do nothing if connection lost
			}
		}

		// process a result recieved from a recovery service
		public void Result(string str, long block) {
			// if pwd found
			if (str != null) {
				decryptedPwd = str;
				finished = true;
				completedIterations = totalIterations;
			}
			// remove block from search space
			else {
				lock (lockObject) {
					blockQueue.Remove(block);
				}
				completedIterations += BLOCKSIZE;
			}
		}

		public string GetDecryptedPwd() {
			return decryptedPwd;
		}

		// returns progress normalized to 0-1 
		public double GetProgress() {
			var completed = Convert.ToDouble(completedIterations);
			var total = Convert.ToDouble(totalIterations);
			var ret = completed / total;

			if (ret > 1.0) {
				ret = 1.0;
			}
			return ret;
		}

		public string GetHashedPwd() {
			return hashedPwd;
		}

		public void Cancel() {
			finished = true;
		}

	}

}
