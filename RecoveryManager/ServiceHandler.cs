﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using RecoveryManagerEndpoints;

namespace RecoveryManager {

	/// <summary>
	/// Manages RecoveryServices.
	/// </summary>
	[ServiceBehavior(
		InstanceContextMode = InstanceContextMode.Single,
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	[CallbackBehavior(
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	class ServiceHandler : IServiceHandler {

		private LinkedList<Solver> solverQueue;
		private Dictionary<string, Solver> solverMap;
		private LinkedList<IRecoveryService> serviceQueue;
		private string charset;

		private object lockObject;


		public ServiceHandler() {
			charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			solverQueue = new LinkedList<Solver>();
			solverMap = new Dictionary<string, Solver>();
			serviceQueue = new LinkedList<IRecoveryService>();
			lockObject = new object();
		}

		public void AddRecoveryService() {
			lock (lockObject) {
				serviceQueue.AddLast(OperationContext.Current.GetCallbackChannel<IRecoveryService>());
				DispatchRecoveryServices();
			}
		}

		public void RemoveRecoveryService() {
			lock (lockObject) {
				serviceQueue.Remove(OperationContext.Current.GetCallbackChannel<IRecoveryService>());
			}
		}

		// give the result to the corresponding Solver if it exists then put the service on the queue and dispatch
		public void ServicePutResult(string hashedPwd, string result, long block) {
			lock (lockObject) {
				if (hashedPwd != null && solverMap.ContainsKey(hashedPwd)) {
					solverMap[hashedPwd].Result(result, block);
				}
				// if ServiceHandler does not have a handle to this RecoveryService, add one
				if (!serviceQueue.Contains(OperationContext.Current.GetCallbackChannel<IRecoveryService>())) {
					serviceQueue.AddLast(OperationContext.Current.GetCallbackChannel<IRecoveryService>());
				}
				DispatchRecoveryServices();
			}
		}

		// if a solver doesn't already exist for this hash, create one
		// then dispatch any services
		public void Decrypt(string hashedPwd, uint pwdLength) {
			if (hashedPwd != null && pwdLength != 0) {
				lock (lockObject) {
					if (!solverMap.ContainsKey(hashedPwd)) {
						var solver = new Solver(hashedPwd, charset, pwdLength);
						solverMap.Add(hashedPwd, solver);
						solverQueue.AddLast(solver);
					}
					DispatchRecoveryServices();
				}
			}
		}

		// return the decrypted pwd and remove Solver if Solver for hashedPwd exists
		// else return null
		public string GetDecryptedPwd(string hashedPwd) {
			string pwd = null;

			lock (lockObject) {
				if (hashedPwd != null && solverMap.ContainsKey(hashedPwd)) {
					var solver = solverMap[hashedPwd];
					pwd = solver.GetDecryptedPwd();

					if (pwd != null) {
						solverMap.Remove(hashedPwd);
						solverQueue.Remove(solver);
					}
				}
			}
			return pwd;
		}

		public double GetProgess(string hashedPwd) {
			if (hashedPwd != null && solverMap.ContainsKey(hashedPwd)) {
				return solverMap[hashedPwd].GetProgress();
			}
			return -1.0;
		}

		// cancel a specific hash decryption if it exists
		public void Cancel(string hashedPwd) {
			lock (lockObject) {
				if (hashedPwd != null && solverMap.ContainsKey(hashedPwd)) {
					var solver = solverMap[hashedPwd];
					solver.Cancel();

					solverMap.Remove(hashedPwd);
					solverQueue.Remove(solver);
				}
			}
		}


		// while there are services available and solvers requiring a service
		// give a service to the solver at the head of the queue
		private void DispatchRecoveryServices() {
			while (serviceQueue.Count > 0 && solverQueue.Count > 0) {
				var solver = solverQueue.First.Value;
				solverQueue.RemoveFirst();

				var service = serviceQueue.First.Value;
				serviceQueue.RemoveFirst();

				bool finished = solver.AddRecoveryService(service);
				if (!finished) {
					solverQueue.AddLast(solver);
				}
				else {
					// solver doesn't need the service so put it back on the queue
					serviceQueue.AddLast(service);
				}
			}
		}
	}
}
