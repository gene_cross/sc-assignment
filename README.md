#Parallel Distributed Password Recovery Tool

###Tertiary Study Assignment
Curtin University 2016 - Software Components - Unit Code: COMP4002

**Task:**  
Implement a password recovery tool that uses distributed computing to perform a highly-parallel brute-force search for the password
by splitting up the task into many sub-tasks and allocating each sub-task to a different computer to process.

This software is broken up into three sections:  
The Recovery Manager; which is the controller for the system and serves the endpoints for the other parts of the system,  
The Recovery Service; this is the worker program that runs on multiple machines and does the actual processing, and  
The User Interface; of which two are provided, Windows Presentation Foundation client GUI, and a very basic html web frontend.  

For a more detailed breakdown of the system see the report, (report.pdf).

