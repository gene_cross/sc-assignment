﻿using System;
using System.Threading;

namespace RecoveryService {

	class Program {

		static void Main(string[] args) {

			Console.WriteLine(" << Recovery Service >>\nTrying to connect to Server..");

			var recoveryService = new RecoveryService(args[0]);

			var timeout = new TimeSpan(TimeSpan.MaxValue.Hours, TimeSpan.MaxValue.Minutes, TimeSpan.MaxValue.Seconds - 4);

			while (true) {
				Thread.Sleep(timeout);
				recoveryService.KeepAlive();
			}
		}
	}
}
