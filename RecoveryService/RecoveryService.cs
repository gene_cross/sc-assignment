﻿using System;
using System.Text;
using System.ServiceModel;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using RecoveryManagerPasswordService;
using RecoveryManagerEndpoints;

namespace RecoveryService {

	[ServiceBehavior(
		InstanceContextMode = InstanceContextMode.Single,
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	[CallbackBehavior(
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	public class RecoveryService : IRecoveryService {

		private delegate string StartDecryptionDelegate(string hashedPwd, string start, long iterations, string charset);
		private delegate void ServicePutResultDelegate(string hashedPwd, string current, long searchStartBlock);

		private NetTcpBinding tcpBinding;
		private IRecoveryServiceCallback recoveryManager;


		// connect to recovery manager
		public RecoveryService(string addr) {
			tcpBinding = new NetTcpBinding();

			tcpBinding.Security.Mode = SecurityMode.None;
			tcpBinding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
			tcpBinding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.None;
			tcpBinding.Security.Message.ClientCredentialType = MessageCredentialType.None;

			tcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
			tcpBinding.MaxReceivedMessageSize = Int32.MaxValue;
			tcpBinding.ReaderQuotas.MaxArrayLength = Int32.MaxValue;

			try {
				var recoveryManagerFactory = new DuplexChannelFactory<IRecoveryServiceCallback>(this, tcpBinding, new EndpointAddress(addr));
				recoveryManager = recoveryManagerFactory.CreateChannel();
				recoveryManager.AddService();
				Console.WriteLine("Connected to server.");
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
			}
		}

		~RecoveryService() {
			try {
				recoveryManager.RemoveService();
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
			}
		}

		public void Decrypt(string hashedPwd, uint pwdLength, long startBlock, long iterations, string charset) {
			if ((hashedPwd == null) || (pwdLength == 0) || (startBlock < 0) || (iterations < 0) || (charset == null)) {
				return;
			}

			string result = null;
			long threadStartBlock = startBlock;
			long iterPerThread = iterations / Environment.ProcessorCount;
			List<IAsyncResult> asyncResultList = new List<IAsyncResult>(Environment.ProcessorCount);

			// start threads and store a handle to each of them
			for (int ii = 0; ii < Environment.ProcessorCount; ii++) {
				StartDecryptionDelegate startDecryptDlg = StartDecryption;

				// int truncation fix, give last thread its iterations plus leftovers
				if (ii == Environment.ProcessorCount - 1) {
					iterPerThread += (iterations % Environment.ProcessorCount) * Environment.ProcessorCount;
				}

				var searchStr = IterationToString(threadStartBlock, Convert.ToUInt32(charset.Length), pwdLength, charset);
				var temp = startDecryptDlg.BeginInvoke(hashedPwd, searchStr, iterPerThread, charset, null, null);

				asyncResultList.Add(temp);
				threadStartBlock += iterPerThread;
			}

			// collect results from each thread, updating result if the solution is found
			StartDecryptionDelegate dlg;
			foreach (var kk in asyncResultList) {
				var res = (AsyncResult)kk;

				if (res.EndInvokeCalled == false) {

					dlg = (StartDecryptionDelegate)res.AsyncDelegate;
					var returnVal = dlg.EndInvoke(res);

					if (returnVal != null) {
						result = returnVal;
					}
				}
			}

			// send the result back
			ServicePutResultDelegate putResultDlg = recoveryManager.ServicePutResult;
			putResultDlg.BeginInvoke(hashedPwd, result, startBlock, null, null);
		}

		// where each thread starts
		private string StartDecryption(string hashedPwd, string start, long iterations, string charset) {
			var current = start.ToCharArray();
			IPwdRecoverer pwdService = new PwdRecoverer();

			// how many iterations this thread is designated to do
			for (long ii = 0; ii < iterations; ii++) {
				if (hashedPwd == pwdService.EncryptString(new string(current))) {
					return new string(current);
				}
				else {
					// get next string to check
					// increments least significant place, then next most significant if needed
					for (int kk = 1; kk <= current.Length; kk++) {
						int index = current.Length - kk;
						// set back to first character if current[index] is last character in set
						if (current[index] == charset[charset.Length - 1]) {
							current[index] = charset[0];
						}
						else {
							current[index] = charset[charset.IndexOf(current[index]) + 1];
							break;
						}
					}
				}
			}
			return null;
		}

		// turns an 'iteration' into a string given a numeric 'base' (number of different values per place, eg: base 10, or base 2)
		private string IterationToString(long iter, uint numBase, uint strLength, string charset) {
			uint ii = strLength - 1u;
			int[] search = new int[strLength];

			while (iter > 0) {
				search[ii] = (int)(iter % numBase);
				iter -= search[ii];
				iter /= numBase;
				ii--;
			}

			StringBuilder str = new StringBuilder(Convert.ToInt32(strLength));
			for (int kk = 0; kk < strLength; kk++) {
				str.Append(charset[search[kk]]);
			}

			return str.ToString();
		}

		// send dummy values to keep tcp channel open
		public void KeepAlive() {
			ServicePutResultDelegate putResultDlg = recoveryManager.ServicePutResult;
			putResultDlg.BeginInvoke(null, null, 0, null, null);
		}

	}
}





