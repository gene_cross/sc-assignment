﻿using System;
using System.ServiceModel;
using RecoveryManagerEndpoints;

public partial class _WebMain : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		lbl_status.Text = "status: Ready";
	}

	protected void btn_getProgress_OnClick(object sender, EventArgs args) {
		Session["addr"] = textBox_ip.Text;

		using (var cf = new ChannelFactory<IRecoveryManager>(new NetTcpBinding(SecurityMode.None), (string)Session["addr"])) {
			var channel = cf.CreateChannel();

			double progress = 0.0;

			try {
				progress = channel.GetProgress((string)Session["hashedPwd"]);
				if (progress == -1) {
					lbl_status.Text = "status: Decryption for this hash has not been started.";
				}
				else if (progress == 1.0) {
					lbl_decryptedPwd.Text = "Decrypted Password: " + channel.GetDecryptedPwd((string)Session["hashedPwd"]);
					lbl_status.Text = "status: Finished decrypting.";
				}
				else {
					lbl_status.Text = "status: Decrypting.. " + (int)(progress * 100) + "%";
				}
			}
			catch (Exception e) {
				lbl_status.Text = "status: " + e.Message;
			}
		}
	}

	protected void btn_getHashedPwd_OnClick(object sender, EventArgs args) {
		Session["addr"] = textBox_ip.Text;

		using (var cf = new ChannelFactory<IRecoveryManager>(new NetTcpBinding(SecurityMode.None), (string)Session["addr"])) {
			var channel = cf.CreateChannel();

			try {
				channel.CancelDecryption((string)Session["hashedPwd"]);

				Session["pwdLength"] = Convert.ToUInt32(textBox_pwdLength.Text);
				Session["hashedPwd"] = channel.GetNextEncryptedPwdToRecover((uint)Session["pwdLength"]);

				if ((string)Session["hashedPwd"] != null) {
					lbl_hashedPwd.Text = "Hashed Password: " + (string)Session["hashedPwd"];
					lbl_decryptedPwd.Text = "Decrypted Password:";
				}
			}
			catch (Exception e) {
				lbl_status.Text = "status: " + e.Message;
			}
		}
	}

	protected void btn_decrypt_OnClick(object sender, EventArgs args) {
		Session["addr"] = textBox_ip.Text;

		using (var cf = new ChannelFactory<IRecoveryManager>(new NetTcpBinding(SecurityMode.None), (string)Session["addr"])) {
			var channel = cf.CreateChannel();

			try {
				channel.Decrypt((string)Session["hashedPwd"], (uint)Session["pwdLength"]);
				lbl_status.Text = "status: Decrypting.. 0%  -  Click 'Get Progress'";
			}
			catch (Exception e) {
				lbl_status.Text = "status: " + e.Message;
			}
		}
	}

	protected void btn_cancel_OnClick(object sender, EventArgs args) {
		Session["addr"] = textBox_ip.Text;

		using (var cf = new ChannelFactory<IRecoveryManager>(new NetTcpBinding(SecurityMode.None), (string)Session["addr"])) {
			var channel = cf.CreateChannel();

			try {
				lbl_status.Text = "status: Canceled. " + (string)Session["hashedPwd"];
				channel.CancelDecryption((string)Session["hashedPwd"]);
			}
			catch (Exception e) {
				lbl_status.Text = "status: " + e.Message;
			}
		}
	}
}