﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="webMain.aspx.cs" Inherits="_WebMain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
	<title>SC-Assignment - Password Recover</title>
</head>

<body>
	<form id="frmInput" runat="server">
		<asp:Label ID="lbl_title" runat="server" Text="<< Parallel Distributed Recovery Tool Web Client >>"></asp:Label>
		<p />
		<asp:Label ID="lbl_enterIP" runat="server" Text="Enter the server IP in the text box below."></asp:Label>
		<br />
		<asp:TextBox ID="textBox_ip" runat="server" Text="net.tcp://localhost:8080/RecoveryManager"></asp:TextBox>
		<p />
		<asp:Label ID="lbl_hashedPwd" runat="server" Text="Hashed Password:"></asp:Label>
		<br />
		<asp:Label ID="lbl_decryptedPwd" runat="server" Text="Decrypted Password:"></asp:Label>
		<p/>
		<asp:TextBox ID="textBox_pwdLength" runat="server" Text="Enter Password Length"></asp:TextBox>
		<p/>
		<asp:Button ID="btn_getProgress" runat="server" Text="Get Progress" OnClick="btn_getProgress_OnClick" />
		<br />
		<asp:Button ID="btn_getHashedPwd" runat="server" Text="Get Hashed Password" OnClick="btn_getHashedPwd_OnClick" />
		<br />
		<asp:Button ID="btn_decrypt" runat="server" Text="Start Decryption" OnClick="btn_decrypt_OnClick" />
		<br />
		<asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_OnClick" />
		<p/>
		<asp:Label ID="lbl_status" runat="server" Text="status: ready"></asp:Label>
	</form>
</body>

</html>


