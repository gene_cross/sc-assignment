﻿using System.ServiceModel;

namespace RecoveryManagerPasswordService {

	[ServiceContract]
	public interface IPwdRecoverer {

		[OperationContract]
		string GetNextEncryptedPwdToRecover(int pwdLength);

		[OperationContract]
		string EncryptString(string pwdString);
	}
}
