﻿using System.ServiceModel;

namespace RecoveryManagerPasswordService {

	[CallbackBehavior(
		ConcurrencyMode = ConcurrencyMode.Multiple,
		UseSynchronizationContext = false,
		IncludeExceptionDetailInFaults = true
	)]
	public class PwdRecoverer : IPwdRecoverer {

		private PwdRecover.PasswordServices pwdService;

		public PwdRecoverer() {
			pwdService = new PwdRecover.PasswordServices();
		}

		public string EncryptString(string pwdString) {
			 return pwdService.EncryptString(pwdString);
		}

		public string GetNextEncryptedPwdToRecover(int pwdLength) {
			return pwdService.NextEncryptedPasswordToRecover(pwdLength);
		}
	}
}
